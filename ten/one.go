package ten

import (
	"context"
)

func one() int {
	ctx := context.Background()
	threes := numbers(ctx, 3)
	fives := numbers(ctx, 5)

	pipe := combine(ctx, threes, fives)

	sum := 0
	for elem := range pipe {
		if elem >= 1000 {
			break
		}
		sum += elem
	}

	return sum
}

func combine(ctx context.Context, a <-chan int, b <-chan int) <-chan int {
	out := make(chan int, 1)
	n := <-a
	m := <-b
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			default:
				if n == m {
					out <- n
					n = <-a
					m = <-b
				} else if n < m {
					out <- n
					n = <-a
				} else {
					out <- m
					m = <-b
				}
			}
		}
	}()
	return out
}

func numbers(ctx context.Context, num int) <-chan int {
	out := make(chan int, 1)
	go func() {
		defer close(out)
		i := num
		for {
			select {
			case <-ctx.Done():
				return
			default:
				out <- i
				i += num
			}
		}
	}()
	return out
}
