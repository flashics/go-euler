package ten

import (
	"math"
)

type item struct {
	peek int
	gen  <-chan int
}

type items []item

func three() int {
	target := int(math.Sqrt(600851475143)) + 1
	primes := primes()
	largest := 0
	for next := <-primes; next < target; next = <-primes {
		if 600851475143%next == 0 {
			largest = next
		}
	}
	return largest
}

func (i *items) next() int {
	next := (*i)[0].peek
	(*i)[0] = item{<-(*i)[0].gen, (*i)[0].gen}

	if len(*i) > 1 {
		for idx := 0; idx < len(*i)-1; idx += 1 {
			if (*i)[idx].peek < (*i)[idx+1].peek {
				break
			} else if (*i)[idx].peek == (*i)[idx+1].peek {
				(*i)[idx+1] = item{<-(*i)[idx+1].gen, (*i)[idx+1].gen}
			} else {
				(*i)[idx], (*i)[idx+1] = (*i)[idx+1], (*i)[idx]
			}
		}
	}

	return next
}

func primes() <-chan int {
	out := make(chan int, 1)
	var fs items

	gen := multiples(3)
	fs = append(fs, item{<-gen, gen})
	go func() {
		out <- 2
		out <- 3
		candidate := 5
		f := fs.next()
		for {
			if candidate < f {
				prime := candidate
				out <- prime

				gen := multiples(prime)
				fs = append(fs, item{<-gen, gen})
			} else {
				f = fs.next()
			}

			candidate += 2
		}
	}()

	return out
}

func multiples(num int) <-chan int {
	out := make(chan int, 1)
	go func() {
		a := num * num
		out <- a
		for {
			a += (num * 2)
			out <- a
		}
	}()
	return out
}
