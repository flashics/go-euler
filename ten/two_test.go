package ten

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestTwo(t *testing.T) {
	v := two()
	fmt.Printf("Euler 2: %d\n", v)
}

func TestFibonacci(t *testing.T) {
	expected := []int{1, 1, 2, 3, 5, 8, 13}
	actual := make([]int, 0, len(expected))

	pipe := fibonacci()

	for i := range pipe {
		actual = append(actual, i)
		if i > 8 {
			break
		}
	}

	assert.Equal(t, expected, actual)
}
