package ten

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestOne(t *testing.T) {
	v := one()
	fmt.Printf("Euler 1: %d\n", v)
}

func TestNumbers(t *testing.T) {

	expected := []int{3, 6, 9, 12, 15, 18}
	actual := make([]int, 0, len(expected))

	ctx := context.Background()
	c := numbers(ctx, 3)

	for i := range c {
		actual = append(actual, i)
		if i > 15 {
			break
		}
	}

	assert.Equal(t, expected, actual)
}

func TestCombine(t *testing.T) {
	expected := []int{3, 5, 6, 9, 10, 12, 15, 18, 20, 21, 24, 25, 27}
	actual := make([]int, 0, len(expected))

	ctx := context.Background()
	a := numbers(ctx, 3)
	b := numbers(ctx, 5)
	c := combine(ctx, a, b)

	for i := range c {
		actual = append(actual, i)
		if i > 25 {
			break
		}
	}
	assert.Equal(t, expected, actual)
}
