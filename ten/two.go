package ten

func two() int {

	sum := 0
	pipe := even(fibonacci())
	for i := range pipe {
		if i > 4000000 {
			break
		}
		sum += i
	}

	return sum
}

func even(in <-chan int) <-chan int {
	out := make(chan int, 1)
	go func() {
		for {
			a := <-in
			if a%2 == 0 {
				out <- a
			}
		}
	}()

	return out
}

func fibonacci() <-chan int {
	out := make(chan int, 1)
	prev := 1
	next := prev
	out <- next
	go func() {
		for {
			out <- next
			prev, next = next, next+prev
		}
	}()

	return out
}
